export const createRootElement = async (target) => {
  const root = document.createElement("div");
  root.setAttribute("id", `${target}`);
  await document.querySelector(`#root`).appendChild(root);
};