import ReactDOM from "react-dom";
import { Footer } from "./Footer";

import { frameworkUtils } from "../../FrameworkUtils/dist/bundle";

const render = frameworkUtils.start({
  renderer: ReactDOM,
  Component: Footer,
  rootElement: "footer",
});

export { render };
