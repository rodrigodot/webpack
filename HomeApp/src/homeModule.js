import ReactDOM from "react-dom";
import { Home } from "./Home";

import { frameworkUtils } from "../../FrameworkUtils/dist/bundle";

const render = frameworkUtils.start({
  renderer: ReactDOM,
  Component: Home,
  rootElement: "content",
});

export { render };
