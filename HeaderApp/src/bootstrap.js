import ReactDOM from "react-dom";

import { Header } from "./Header";

ReactDOM.render(<Header />, document.querySelector("#header"));
