import { BrowserRouter as Router, Link } from "react-router-dom";
import css from "./header.module.css";

const Header = () => {
  return (
    <Router>
      <nav className={css.headerContainer}>
        <button className={css.headerButton}>
          <Link to="/">Home</Link>
        </button>
        <button className={css.headerButton}>
          <Link to="/about">Sobre</Link>
        </button>
      </nav>
    </Router>
  );
};

export { Header };
