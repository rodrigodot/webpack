import css from './home.module.css'

const Home = () => {
  return (
    <div className={css.homeContainer}>
      <h1>Home app!</h1>
    </div>
  );
};

export { Home }
