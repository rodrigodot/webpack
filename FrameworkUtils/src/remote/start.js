export const start = ({ renderer, Component, rootElement }) => {
  return () =>
    renderer.render(<Component />, document.querySelector(`#${rootElement}`));
};
