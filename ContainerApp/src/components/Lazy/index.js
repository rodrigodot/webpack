export default (props = {}) => React.lazy(() => {
  const { resource, ...rest } = props
  console.log('RESOURCE >>> ', resource)

  return import(`${resource}`).catch((e) => {
    console.error('IMPORT ERROR >>>> ', e);

    return {
      default: () => <h1 {...rest}>Oops!!!</h1>,
    };
  });
});