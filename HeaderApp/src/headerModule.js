import ReactDOM from "react-dom";
import { Header } from "./Header";

import { frameworkUtils } from "../../FrameworkUtils/dist/bundle";

const render = frameworkUtils.start({
  renderer: ReactDOM,
  Component: Header,
  rootElement: "header",
});

export { render };
