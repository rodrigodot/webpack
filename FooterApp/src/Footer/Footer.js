import css from './footer.module.css'

const Footer = () => {
  return (
    <div className={css.footerContainer}>
      <span className={css.footerText}>Footer app!</span>
    </div>
  );
};

export { Footer };
