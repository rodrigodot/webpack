import { Footer } from "./Footer";
import ReactDOM from "react-dom";

ReactDOM.render(<Footer />, document.querySelector("#root"));
