import { frameworkUtils } from "../../FrameworkUtils/dist/bundle";
import "./global.css";

const pushState = history.pushState;
history.pushState = function () {
  pushState.apply(history, arguments);
  routerWatcher("pushState", arguments);
};

function initialize() {
  import(`HeaderApp/HeaderApp`).then((HeaderApp) => {
    console.log("HeaderApp loaded");
    HeaderApp?.render();
    console.log("HeaderApp rendered");
  });

  import("FooterApp/FooterApp").then((FooterApp) => {
    console.log("FooterApp loaded");
    FooterApp.render();
    console.log("FooterApp rendered");
  });

  const { pathname } = new URL(window.location);
  history.pushState({ ...history.state }, document.title, pathname);
}

function routerWatcher() {
  if (window.location.pathname.endsWith("/")) {
    frameworkUtils.createRootElement("content");

    import("HomeApp/HomeApp").then((HomeApp) => {
      console.log("HomeApp loaded");
      HomeApp.render();
      console.log("HomeApp rendered");
    });
  } else {
    const root = document.querySelector("#content");
    if (root) root.remove();
  }

  if (window.location.pathname.startsWith("/about")) {
    frameworkUtils.createRootElement("content");

    import("AboutApp/AboutApp").then((AboutApp) => {
      console.log("AboutApp loaded");
      AboutApp.render();
      console.log("AboutApp rendered");
    });
  } else {
    const root = document.querySelector("#content");
    if (root) root.remove();
  }
}

initialize();
