import ReactDOM from "react-dom";
import { About } from "./About";

import { frameworkUtils } from "../../FrameworkUtils/dist/bundle";

const render = frameworkUtils.start({
  renderer: ReactDOM,
  Component: About,
  rootElement: "content",
});

export { render };
