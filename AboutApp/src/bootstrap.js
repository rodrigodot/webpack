import ReactDOM from "react-dom";

import { About } from "./About";

ReactDOM.render(<About />, document.querySelector("#root"));
