import css from './about.module.css'

const About = () => {
  return (
    <div className={css.aboutContainer}>
      <h1>About app!</h1>
    </div>
  );
};

export { About };
